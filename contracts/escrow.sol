pragma solidity >=0.4.22 <0.6.0;

contract Escrow {
    /// 진행 상황에 따라 변화하는 상태 정보를 의미한다.
    enum State {
        INIT,       // 0
        CANCELLED,  // _1
        ACTIVATED,  // 2
        REWARD_DEPOSITTED,  // 3
        PASSWORD_SHARED,    // 4
        REWARD_GRANTED     // _5
    }


    /// 소유한 QR-Code 목록
    struct OwnedList {
        QRCode[] ownedTags;
    }

    /// QR-Code에 대한 상태 정보
    struct QRCode {
        bytes tagHash; // h( h(secret) + {owner, estimatedReward, id})
        address owner;
        uint256 estimatedReward;
        bytes id;
        //bytes32 keyStick; // 비밀번호를 풀 때 같이 사용되는 값. QR에만 존재한다.
        bytes hSecret; // 비밀번호를 풀 때 쓰는 값,
        uint256 height;
        int state;
    }


    /// 아이디 - 태그들로 묶인 정보들
    mapping(address=>OwnedList) owners;

    /// tagId를 기준으로 묶여있는 Tag 정보들
    mapping(bytes=>QRCode) qrs;


    /// Finders mapped on tagHash
    mapping(bytes => address) finders;

    mapping(bytes => bool) passwordNotified;


    constructor() public { }

    /// 태그를 생성했을 때 발생하는 이벤트
    event TagGenerated(address generator, bytes tagId);

    /// 사용자 소지품을 습득자가 발견했을 때 발생하는 이벤트
    event LostFound(bytes tagHash, bytes secret, address finder, string location);

    /// 분실물을 확인하기 위한 신호 발신 이벤트
    event ValidationSignal(bytes tagHash, int signalType, int signalBits);

    /// 보상을 전달할 수 있도록 예치하는 이벤트
    event RewardDeposited(bytes tagHash, address finder);

    // 비밀번호를 전달할 때 발생하는 이벤트
    event PasswordGranted(bytes tagHash, address finder, string password);

    /// 태그에 대한 보상을 지급했을 때 발생하는 이벤트
    event RewardGranted(bytes tagHash, address owner, address finder);


    /// QR코드의 주인 여부에 대한 제어자
    modifier isQROwner(bytes memory tagHash) {
        bytes32 target = keccak256(abi.encodePacked(tagHash));
        bool isRight = false;
        OwnedList memory list = owners[msg.sender];
        for (int i = 0; i < int(list.ownedTags.length); i++) {
            bytes memory testcase = list.ownedTags[uint(i)].tagHash;
            bytes32 tcHash = keccak256(abi.encodePacked(testcase));
            if (target == tcHash) isRight = true;
        }
        require(isRight);
        _;
    }

    /// 해당 QR코드를 이용하여 보상받을 권리를 가진 사용자 확인에 대한 제어자
    modifier isFinder(bytes tagHash) {
        require(finders[tagHash] == msg.sender);
        _;
    }

    modifier isInTransferenceState(address registeredAddress) {
        require(msg.sender == registeredAddress);
        _;
    }


    /// fallback function
    function() public payable { }

    /// 특정 QR코드에 대한 현재 상태를 본다.
    function currentState(bytes _tagHash) public view returns (int) {
        return qrs[_tagHash].state;
    }


    /// admin tools
    function getMiddleManBalance() public view returns (uint256) {
        return address(this).balance;
    }


    /// QR-Code 등록 함수
    function registerQR(bytes _id, uint _estimatedReward, bytes _hSecret, bytes _tagHash) public payable {
        // bytes memory byteshashedSecret = bytes(hSecret);
        QRCode memory code = QRCode(_tagHash, msg.sender, _estimatedReward, _id, _hSecret, 0, int(State.INIT));
        qrs[_tagHash] = code;
    }

    /// 분실물 획득 함수
    function notifyLostFound(bytes _tagHash, string _secret, string _locataion) public payable {
        // semiTagHash == secret
        // semiTagHash = h(secret + owner + estimatedReward + id)
        bytes memory secret = bytes(_secret);
        int current = qrs[_tagHash].state;
        if (current == int(State.INIT) || current == int(State.CANCELLED)) {
            qrs[_tagHash].state = int(State.ACTIVATED);
        }
        finders[_tagHash] = msg.sender;

        emit LostFound(_tagHash, secret, msg.sender, _locataion);
    }

    /// 분실물 확인 함수
    function sendSignal(bytes _tagHash, int _signalType, int _signalBits) public payable {
        emit ValidationSignal(_tagHash, _signalType, _signalBits);
    }

    /// 보상 예치 함수
    function depositReward(bytes tagHash) public payable {
        require(msg.value == qrs[tagHash].estimatedReward);
        address finder = finders[tagHash];
        address(this).transfer(msg.value);
        emit RewardDeposited(tagHash, finder);
    }

    /// 비밀번호 제공 함수
    function notifyPassword(bytes _tagHash, string _password) public {
        QRCode memory qr = qrs[_tagHash];
        qr.height = block.number;
        qr.state = int(State.PASSWORD_SHARED);
        passwordNotified[_tagHash] = true;
        emit PasswordGranted(_tagHash, msg.sender, _password);
    }

    /// 보상을 전달하는 함수
    function grantReward(bytes _tagHash) public {
        address finder = finders[_tagHash];
        QRCode memory qr = qrs[_tagHash];
        qr.state = int(State.REWARD_DEPOSITTED);
        finder.transfer(qr.estimatedReward);
        delete finders[_tagHash];

        emit RewardGranted(_tagHash, msg.sender, finder);
    }

    /// 소유주가 일정 시간이 지나도 권리를 지급하지 않는 경우 수행가능
    function fetchReward(bytes _tagHash) public payable {
        require(passwordNotified[_tagHash]);
        require(block.number > 8000 + qrs[_tagHash].height);
        QRCode memory qr = qrs[_tagHash];
        qr.state = int(State.REWARD_GRANTED);
        address finder = finders[_tagHash];
        finder.transfer(qr.estimatedReward);
        delete finders[_tagHash];
    }

    function QR(bytes _tagHash) public view returns (bytes, address, uint256, bytes, bytes, uint256, int) {
        QRCode memory qr = qrs[_tagHash];
        return (qr.tagHash, qr.owner, qr.estimatedReward, qr.id, qr.hSecret, qr.height, qr.state);
    }
}
